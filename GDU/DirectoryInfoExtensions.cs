﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace GDU
{
	static class DirectoryInfoExtensions
	{
		public static IEnumerable<FileInfo> EnumerateFilesSafe(this DirectoryInfo directoryInfo)
		{
			try
			{
				return directoryInfo.EnumerateFiles();
			}
			catch (Exception)
			{
				return Enumerable.Empty<FileInfo>();
			}
		}

		public static IEnumerable<DirectoryInfo> EnumerateDirectoriesSafe(this DirectoryInfo directoryInfo)
		{
			try
			{
				return directoryInfo.EnumerateDirectories();
			}
			catch (Exception)
			{
				return Enumerable.Empty<DirectoryInfo>();
			}
		}
	}
}