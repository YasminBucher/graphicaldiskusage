﻿using System.IO;

namespace GDU
{
	class File : IStorageObject
	{
		private FileInfo _fileInfo;

		public File(FileInfo fileInfo)
		{
			_fileInfo = fileInfo;
		}

		public long GetSize()
		{
			return _fileInfo.Length;
		}

		public override string ToString()
		{
			return string.Format("{0} ({1})", _fileInfo.Name, Utility.ConvertByteString(this.GetSize()));
		}
	}
}