﻿namespace GDU
{
	partial class MainForm
	{
		/// <summary>
		/// Erforderliche Designervariable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Verwendete Ressourcen bereinigen.
		/// </summary>
		/// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
		protected override void Dispose(bool disposing)
		{
			if(disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Vom Windows Form-Designer generierter Code

		/// <summary>
		/// Erforderliche Methode für die Designerunterstützung.
		/// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnSelectDirectory = new System.Windows.Forms.Button();
			this.tvDirectories = new System.Windows.Forms.TreeView();
			this.btnScan = new System.Windows.Forms.Button();
			this.txtDirectory = new System.Windows.Forms.TextBox();
			this.lblNowScanning = new System.Windows.Forms.Label();
			this.lblNowScanningOutput = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// btnSelectDirectory
			// 
			this.btnSelectDirectory.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnSelectDirectory.Location = new System.Drawing.Point(694, 10);
			this.btnSelectDirectory.Name = "btnSelectDirectory";
			this.btnSelectDirectory.Size = new System.Drawing.Size(35, 23);
			this.btnSelectDirectory.TabIndex = 0;
			this.btnSelectDirectory.Text = "...";
			this.btnSelectDirectory.UseVisualStyleBackColor = true;
			this.btnSelectDirectory.Click += new System.EventHandler(this.btnSelectDirectory_Click);
			// 
			// tvDirectories
			// 
			this.tvDirectories.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.tvDirectories.Location = new System.Drawing.Point(12, 51);
			this.tvDirectories.Name = "tvDirectories";
			this.tvDirectories.Size = new System.Drawing.Size(776, 387);
			this.tvDirectories.TabIndex = 1;
			// 
			// btnScan
			// 
			this.btnScan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnScan.Location = new System.Drawing.Point(735, 10);
			this.btnScan.Name = "btnScan";
			this.btnScan.Size = new System.Drawing.Size(53, 23);
			this.btnScan.TabIndex = 2;
			this.btnScan.Text = "Scan";
			this.btnScan.UseVisualStyleBackColor = true;
			this.btnScan.Click += new System.EventHandler(this.btnScan_Click);
			// 
			// txtDirectory
			// 
			this.txtDirectory.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.txtDirectory.Location = new System.Drawing.Point(12, 12);
			this.txtDirectory.Name = "txtDirectory";
			this.txtDirectory.Size = new System.Drawing.Size(676, 20);
			this.txtDirectory.TabIndex = 3;
			// 
			// lblNowScanning
			// 
			this.lblNowScanning.AutoSize = true;
			this.lblNowScanning.Location = new System.Drawing.Point(12, 35);
			this.lblNowScanning.Name = "lblNowScanning";
			this.lblNowScanning.Size = new System.Drawing.Size(78, 13);
			this.lblNowScanning.TabIndex = 4;
			this.lblNowScanning.Text = "Now scanning:";
			// 
			// lblNowScanningOutput
			// 
			this.lblNowScanningOutput.AutoEllipsis = true;
			this.lblNowScanningOutput.Location = new System.Drawing.Point(96, 35);
			this.lblNowScanningOutput.Name = "lblNowScanningOutput";
			this.lblNowScanningOutput.Size = new System.Drawing.Size(692, 13);
			this.lblNowScanningOutput.TabIndex = 5;
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(800, 450);
			this.Controls.Add(this.lblNowScanningOutput);
			this.Controls.Add(this.lblNowScanning);
			this.Controls.Add(this.txtDirectory);
			this.Controls.Add(this.btnScan);
			this.Controls.Add(this.tvDirectories);
			this.Controls.Add(this.btnSelectDirectory);
			this.Name = "MainForm";
			this.Text = "GDU";
			this.Resize += new System.EventHandler(this.MainForm_Resize);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button btnSelectDirectory;
		private System.Windows.Forms.TreeView tvDirectories;
		private System.Windows.Forms.Button btnScan;
		private System.Windows.Forms.TextBox txtDirectory;
		private System.Windows.Forms.Label lblNowScanning;
		private System.Windows.Forms.Label lblNowScanningOutput;
	}
}

