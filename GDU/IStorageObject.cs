﻿namespace GDU
{
	interface IStorageObject
	{
		long GetSize();
	}
}