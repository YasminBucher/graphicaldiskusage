﻿namespace GDU
{
	class Utility
	{
		public static string ConvertByteString(long value)
		{
			int c = 0;

			while(value > 1024)
			{
				value /= 1024;
				c++;
			}

			return string.Format("{0}{1}", value, ((ByteSizeEnum)c).ToString());
		}
	}
}