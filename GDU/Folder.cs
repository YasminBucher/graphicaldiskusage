﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace GDU
{
	class Folder : IStorageObject
	{
		public static EventHandler<string> ReportCurrentDirectory;

		private string _path;
		private List<Folder> _folders;
		private List<File> _files;

		private bool _sizeAlreadyScanned = false;
		private long _size = 0;

		public Folder(string path)
		{
			this._path = path;
			DirectoryInfo di = new DirectoryInfo(path);
			this._folders = new List<Folder>();
			this._files = new List<File>();

			if (ReportCurrentDirectory != null)
				ReportCurrentDirectory.Invoke(this, path);

			this._files.AddRange(di.EnumerateFilesSafe().Select(fi => new File(fi)));
			this._folders.AddRange(di.EnumerateDirectoriesSafe().Where(dir => !dir.Attributes.HasFlag(FileAttributes.ReparsePoint)).Select(diSub => new Folder(diSub.FullName)));
			this._files.Sort(MainForm.COMPARER);
			this._folders.Sort(MainForm.COMPARER);
		}

		public long GetSize()
		{
			if (!this._sizeAlreadyScanned)
			{
				foreach(File file in _files)
				{
					this._size += file.GetSize();
				}

				foreach(Folder folder in _folders)
				{
					this._size += folder.GetSize();
				}

				this._sizeAlreadyScanned = true;
			}

			return this._size;
		}

		public bool HasSubfolders()
		{
			return _folders.Count > 0;
		}

		public List<Folder> GetFolders()
		{
			return _folders;
		}

		public List<File> GetFiles()
		{
			return _files;
		}

		public override string ToString()
		{
			return string.Format("{0} ({1})", this._path, Utility.ConvertByteString(this.GetSize()));
		}
	}
}