﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDU
{
	class StorageObjectComparer : IComparer<IStorageObject>
	{
		public int Compare(IStorageObject x, IStorageObject y)
		{
			if (x.GetSize() > y.GetSize())
				return 1;
			else if (y.GetSize() > x.GetSize())
				return -1;
			else
				return 0;
		}
	}
}