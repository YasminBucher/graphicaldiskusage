﻿using GDU.Controls;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GDU
{
	public partial class MainForm : Form
	{
		internal static readonly StorageObjectComparer COMPARER = new StorageObjectComparer();

		Image imgLoading;
		PanelExtended pnlLoadingBlocker;
		Panel pnlLoading;
		PictureBox pbLoading;
		Button btnCancelLoading;
		Task scanTask;
		CancellationTokenSource cancellationTokenSource;
		CancellationToken cancellationToken;
		List<Folder> folders = new List<Folder>();

		public MainForm()
		{
			InitializeComponent();
			Folder.ReportCurrentDirectory += FolderReportCurrentDirectory;
			this.InitLoadingScreen();
			this.cancellationTokenSource = new CancellationTokenSource();
			this.cancellationToken = this.cancellationTokenSource.Token;
		}

		private void InitLoadingScreen()
		{
			this.imgLoading = new Bitmap(Path.Combine(Application.StartupPath, "img", "loader.gif"));
			this.btnCancelLoading = new Button();
			this.pbLoading = new PictureBox();
			this.pnlLoadingBlocker = new PanelExtended();
			this.pnlLoading = new Panel();

			this.pbLoading.Location = new Point(0, 0);
			this.pbLoading.Image = this.imgLoading;
			this.pbLoading.Size = new Size(200, 200);
			this.pbLoading.SizeMode = PictureBoxSizeMode.CenterImage;

			this.btnCancelLoading.Text = "Cancel";
			this.btnCancelLoading.Location = new Point((this.pbLoading.Width / 2) - (this.btnCancelLoading.Width / 2), (this.pbLoading.Height / 2) - (this.btnCancelLoading.Height / 2));
			this.btnCancelLoading.Click += BtnCancelLoading_Click;

			this.pnlLoading.Name = "pnlLoading";
			this.pnlLoading.Size = new Size(this.pbLoading.Width, this.pbLoading.Height);
			this.pnlLoading.BorderStyle = BorderStyle.Fixed3D;
			this.pnlLoading.Controls.Add(this.btnCancelLoading);
			this.pnlLoading.Controls.Add(this.pbLoading);

			this.pnlLoadingBlocker.Location = new Point(0, 0);
			this.pnlLoadingBlocker.Name = "pnlLoadingBlocker";
			this.pnlLoadingBlocker.Size = new Size(1, 1);
			this.pnlLoadingBlocker.Dock = DockStyle.Fill;
			this.pnlLoadingBlocker.Opacity = 0.5f;
			this.pnlLoadingBlocker.Controls.Add(this.pnlLoading);

			this.Controls.Add(this.pnlLoadingBlocker);
			this.pnlLoadingBlocker.BringToFront();
			this.pnlLoading.Location = new Point((this.Width / 2) - (this.pnlLoading.Width / 2), (this.Height / 2) - (this.pnlLoading.Height / 2));

			this.pnlLoadingBlocker.Visible = false;
		}

		private void FolderReportCurrentDirectory(object sender, string e)
		{
			this.SetNowScanning(e);
		}

		private void SetNowScanning(string path)
		{
			if (this.lblNowScanningOutput.InvokeRequired)
				this.lblNowScanningOutput.Invoke(new Action(() => { this.SetNowScanning(path); }));
			else
				this.lblNowScanningOutput.Text = path;
		}

		private void BtnCancelLoading_Click(object sender, EventArgs e)
		{
			this.cancellationTokenSource.Cancel();
		}

		private void btnSelectDirectory_Click(object sender, EventArgs e)
		{
			FolderBrowserDialog fbd = new FolderBrowserDialog();
			fbd.ShowNewFolderButton = false;

			DialogResult dr = fbd.ShowDialog();

			if (dr == DialogResult.OK)
			{
				this.txtDirectory.Text = fbd.SelectedPath;
			}
		}

		private void btnScan_Click(object sender, EventArgs e)
		{
			if (this.ValidateDirectorySelection())
			{
				this.pnlLoadingBlocker.Visible = true;
				this.scanTask = Task.Factory.StartNew(() => this.Scan(this.txtDirectory.Text), this.cancellationToken).ContinueWith(t => this.UpdateGui(), TaskScheduler.FromCurrentSynchronizationContext());
			}
		}

		private void Scan(string directory)
		{
			this.folders.Clear();

			if(directory == string.Empty)
			{
				foreach (DriveInfo driveInfo in DriveInfo.GetDrives())
				{
					if(driveInfo.DriveType == DriveType.Fixed && driveInfo.IsReady)
					{
						Folder f = new Folder(driveInfo.RootDirectory.FullName);
						this.folders.Add(f);
					}
				}
			}
			else
			{
				Folder f = new Folder(directory);
				this.folders.Add(f);
			}
		}

		private void UpdateGui()
		{
			foreach (Folder folder in this.folders)
			{
				this.AddFolderNodes(folder, this.tvDirectories.Nodes);
			}

			this.SetNowScanning(string.Empty);
			this.pnlLoadingBlocker.Visible = false;
		}

		private void AddFolderNodes(Folder folder, TreeNodeCollection nodes)
		{
			TreeNode node = nodes.Add(folder.ToString());

			foreach (Folder f in folder.GetFolders())
			{
				this.AddFolderNodes(f, node.Nodes);
			}

			foreach (File file in folder.GetFiles())
			{
				node.Nodes.Add(file.ToString());
			}
		}

		private bool ValidateDirectorySelection()
		{
			string dir = this.txtDirectory.Text;

			if (dir == string.Empty)
				return true;

			DirectoryInfo di = new DirectoryInfo(dir);

			return di.Exists;
		}

		private void MainForm_Resize(object sender, EventArgs e)
		{
			this.pnlLoading.Location = new Point((this.Width / 2) - (this.pnlLoading.Width / 2), (this.Height / 2) - (this.pnlLoading.Height / 2));
		}
	}
}