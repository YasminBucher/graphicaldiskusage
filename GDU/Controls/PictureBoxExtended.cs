﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;

namespace GDU.Controls
{
	class PictureBoxExtended : Control
	{
		private const int WS_EX_TRANSPARENT = 0x20;

		private float _opacity;
		private Image _image;
		private FrameDimension _dimension;
		private int _frameCount;
		private int _currentFrame;

		private System.Timers.Timer _timer;

		public float Opacity
		{
			get
			{
				return this._opacity;
			}
			set
			{
				if(value > 1.00 || value < 0.00)
				{
					throw new ArgumentOutOfRangeException("value may be between 0.00 and 1.00");
				}

				this._opacity = value;
			}
		}

		public Image Image
		{
			get
			{
				return this._image;
			}
			set
			{
				this._image = value;

				FrameDimension dimension = new FrameDimension(value.FrameDimensionsList[0]);
				this._dimension = dimension;
				this._frameCount = value.GetFrameCount(dimension);
			}
		}

		public PictureBoxExtended()
		{
			this._timer = new System.Timers.Timer(50);
			this._timer.Elapsed += _timer_Elapsed;
			this._timer.Start();
			this.SetStyle(ControlStyles.Opaque, true);
		}

		private void _timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
		{
			if(this._frameCount > 1)
			{
				this._currentFrame++;

				if(this._currentFrame >= this._frameCount)
				{
					this._currentFrame = 0;
				}

				this.Invalidate();
			}
		}

		protected override void OnBackColorChanged(EventArgs e)
		{
			base.OnBackColorChanged(e);
			this.Invalidate(true);
		}

		protected override CreateParams CreateParams
		{
			get
			{
				CreateParams cpar = base.CreateParams;
				cpar.ExStyle = cpar.ExStyle | WS_EX_TRANSPARENT;
				return cpar;
			}
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			base.OnPaint(e);

			int alpha = (int)Math.Floor(this._opacity * 255.00);
			Color backColor = Color.FromArgb(alpha, this.BackColor);

			//e.Graphics.Clear(Color.FromArgb(120, Color.Purple));

			Brush brush = new SolidBrush(backColor);
			Rectangle rect = this.ClientRectangle;
			e.Graphics.FillRectangle(brush, rect);
			brush.Dispose();

			this._image.SelectActiveFrame(_dimension, _currentFrame);
			e.Graphics.DrawImageUnscaled(this._image, new Point(0, 0));
		}
	}
}