﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace GDU.Controls
{
	class PanelExtended : Panel
	{
		private const int WS_EX_TRANSPARENT = 0x20;

		private float _opacity;

		public float Opacity
		{
			get
			{
				return this._opacity;
			}
			set
			{
				if(value > 1.00 || value < 0.00)
				{
					throw new ArgumentOutOfRangeException("value may be between 0.00 and 1.00");
				}

				this._opacity = value;
			}
		}

		public PanelExtended()
		{
			this.SetStyle(ControlStyles.Opaque, true);
		}

		protected override void OnBackColorChanged(EventArgs e)
		{
			base.OnBackColorChanged(e);
			this.Invalidate(true);
		}

		protected override CreateParams CreateParams
		{
			get
			{
				CreateParams cpar = base.CreateParams;
				cpar.ExStyle = cpar.ExStyle | WS_EX_TRANSPARENT;
				return cpar;
			}
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			int alpha = (int)Math.Floor(this._opacity * 255.00);
			Color backColor = Color.FromArgb(alpha, this.BackColor);

			Brush brush = new SolidBrush(backColor);
			Rectangle rect = this.ClientRectangle;
			e.Graphics.FillRectangle(brush, rect);
			brush.Dispose();

			base.OnPaint(e);
		}
	}
}